### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 974e1edc-f837-41d8-9657-65ad57d85419
begin
	using Pkg
	Pkg.activate("Project.toml")
	using PlutoUI
end

# ╔═╡ d339e7d5-51a5-4d79-8465-88d209947bc4
begin
	using Compose
	using ColorSchemes
	using POMDPs
	using POMDPModels
	using POMDPSimulators
	using POMDPModelTools
	using POMDPPolicies
	using Test
	using POMDPTesting
	using Distributions
	using Parameters
	using BeliefUpdaters
	using BenchmarkTools
	# using PlotUtils
	using Random
	using StaticArrays
	using Statistics
	using StatsBase
	using Printf
	
end

# ╔═╡ 9bab7974-38f5-4b5c-b5b1-4c312dda8303
# md"# Willy Wonker Wuz Here 2k17  is great"
md"# Jose Quenum is the greatest Lecturer Ever !!!"
#Heres to hoping I get 51% for this Assignments......

# ╔═╡ c7d55112-baf5-4a20-96b8-7e38efde74d1
md"## Markov Decision Policy Observation"

# ╔═╡ 9406ecf6-32d1-437a-a702-5043343c9db1
# States, Actions, Rewards, transitions

# ╔═╡ 80578f8e-f6ab-40f1-8c53-927f8374e7dc
#Value Iteration Algorithm

# ╔═╡ 03791fd9-6de2-4330-a3b9-ad916fb90c71
# pomdp = TigerPOMDP()

# ╔═╡ a23bd61c-d379-4bad-9f03-4085fe5e7c94
# solver = QMDPSolver()

# ╔═╡ 99d00db2-1fcf-409e-9555-9fd87c8421e2
# policy = solve(solver, pomdp)

# ╔═╡ 1c243a50-0002-4239-bbc8-3f5e9f4fd861
# with_terminal() do
# 	println(policy)
# end

# ╔═╡ 16afac91-c2b6-43a3-9a5e-3bed5b583c4e
md"# Markov Decision Process"

# ╔═╡ c082a2a5-0e12-4e3a-9cbc-3e154e574fa2
md"## States, World_Model, Actions, Rewards, && Policies"

# ╔═╡ 4f88c935-6f4d-42fd-9433-44fbb07dcdea
md"### State"

# ╔═╡ cf48e641-5d77-4ec7-b231-4e2fadb1c369
const GWPos = SVector{2,Int}

# ╔═╡ 6ec4e27a-8abc-47e6-ab9c-3bf2bd9a21ac
md"# Grid World Definition"

# ╔═╡ 855e26cc-19c0-4e17-962a-e63d2e20d000
@with_kw struct SimpleGridWorld <: MDP{GWPos, Symbol}
    size::Tuple{Int, Int}           = (10,10)
    rewards::Dict{GWPos, Float64}   = Dict(GWPos(4,3)=>-10.0, GWPos(4,6)=>-5.0, GWPos(9,3)=>10.0, GWPos(8,8)=>3.0)
    terminate_from::Set{GWPos}      = Set(keys(rewards))
    tprob::Float64                  = 0.7
    discount::Float64               = 0.60
end

# ╔═╡ 3e38ec49-785f-4b1b-8660-75c5fa08106b
md"## Defining the Grid Worlds Utillities" #State Function

# ╔═╡ 71f09d11-7769-4557-aa09-c36dda1ecb44
md"### State Functions"
# 	Defining the states

# ╔═╡ 75ef83ac-6c7a-4618-9f58-574ac8ecaf5c
function POMDPs.states(mdp::SimpleGridWorld)
    ss = vec(GWPos[GWPos(x, y) for x in 1:mdp.size[1], y in 1:mdp.size[2]])
    push!(ss, GWPos(-1,-1))
    return ss
end

# ╔═╡ aa0e760c-87e0-4184-97ed-dc53976f3151
function POMDPs.stateindex(mdp::SimpleGridWorld, s::AbstractVector{Int})
    if all(s.>0)
        return LinearIndices(mdp.size)[s...]
    else
        return prod(mdp.size) + 1 # TODO: Change
    end
end

# ╔═╡ 71e92ceb-b8f3-4383-b55b-986564ae0d99
begin
		struct GWUniform
    		size::Tuple{Int, Int}
		end
	
Base.rand(rng::AbstractRNG,d::GWUniform)=GWPos(rand(rng,1:d.size[1]),rand(rng, 1:d.size[2]))

Distributions.support(d::GWUniform)=(GWPos(x,y) for x in 1:d.size[1],y in 1:d.size[2])
	
	POMDPs.initialstate(mdp::SimpleGridWorld) = GWUniform(mdp.size) 
	#Return a distribution of initial states
	
end

# ╔═╡ e0659180-b273-4682-8ad1-9a3148ecc229
		function pdf(d::GWUniform, s::GWPos)
    	if all(1 .<= s[1] .<= d.size)
        	return 1/prod(d.size)
    	else
        return 0.0
    	end
	end

# ╔═╡ e3cbe85a-8956-4e54-9f8f-541ceebfa263
md"## Defining Actions "

# ╔═╡ d6523f00-7e4d-4d19-9196-4a3b12c7bbce
begin
		# Actions
	
POMDPs.actions(mdp::SimpleGridWorld) = (:up, :down, :left, :right)
Base.rand(rng::AbstractRNG, t::NTuple{L,Symbol}) where L = t[rand(rng, 1:length(t))] 

		const dir = Dict(:up=>GWPos(0,1),:down=>GWPos(0,-1),:left=>GWPos(-1,0),:right=>GWPos(1,0))
	
		const aind = Dict(:up=>1, :down=>2, :left=>3, :right=>4)
		POMDPs.actionindex(mdp::SimpleGridWorld, a::Symbol) = aind[a]
	
end

# ╔═╡ dae85980-d665-4a84-a1e8-ca17f6e89b5b
md"# Tansition Model"

# ╔═╡ a7bca195-850a-4cb7-8de5-578c716216cf
	# Transitions
POMDPs.isterminal(m::SimpleGridWorld, s::AbstractVector{Int}) = any(s.<0)

# ╔═╡ b68377f6-a930-4c79-b2d3-f67d1950dd69
		function inbounds(m::SimpleGridWorld, s::AbstractVector{Int})
    		return 1 <= s[1] <= m.size[1] && 1 <= s[2] <= m.size[2]
		end

# ╔═╡ a57571ec-e1e0-4ac3-ada0-ed932def0822
		function POMDPs.transition(mdp::SimpleGridWorld, s::AbstractVector{Int}, a::Symbol)
    	if s in mdp.terminate_from || isterminal(mdp, s)
        	return Deterministic(GWPos(-1,-1))
    	end

    	destinations = MVector{length(actions(mdp))+1, GWPos}(undef)
    	destinations[1] = s

    	probs = @MVector(zeros(length(actions(mdp))+1))
		
    	for (i, act) in enumerate(actions(mdp))
			if act == a
            	prob = mdp.tprob 
				# probability of transitioning to the desired cell
        		else
            	prob = (1.0 - mdp.tprob)/(length(actions(mdp)) - 1) 
				# probability of transitioning to another cell
        	end

        dest = s + dir[act]
        destinations[i+1] = dest

        	if !inbounds(mdp, dest) # hit an edge and come back
            	probs[1] += prob
            	destinations[i+1] = GWPos(-1, -1) 
				# dest was out of bounds 
				# - this will have probability zero, but it should be a valid state
        		else
            	probs[i+1] += prob
       		 	end
    		  end
    		return SparseCat(destinations, probs)
		end

# ╔═╡ 37f0a8d7-7395-4db5-8698-79abfbb19ccf
md"# Rewards"

# ╔═╡ c6b13a22-1370-4e96-baaa-8ec3212a1332
begin
POMDPs.reward(mdp::SimpleGridWorld, s::AbstractVector{Int}) = get(mdp.rewards, s, 0.0)
POMDPs.reward(mdp::SimpleGridWorld, s::AbstractVector{Int}, a::Symbol)=reward(mdp, s)
end

# ╔═╡ bacf774f-97c8-4916-8b4b-46072fcef9b9
md"# Discount"

# ╔═╡ 7b235da8-b805-40de-ad2d-b6b5fc4dd36a
POMDPs.discount(mdp::SimpleGridWorld) = mdp.discount

# ╔═╡ f5138e9b-e1be-4b6a-8acf-562cb8c2f8fe
md"# Conversion Functions"

# ╔═╡ 105c60ea-3137-479e-a1ca-ea1f50e169b7
		function convert_a(::Type{V}, a::Symbol, m::SimpleGridWorld) where {V<:AbstractArray}
    convert(V, [aind[a]])
		end
	

# ╔═╡ 837b9a0b-f2b0-4485-b03e-ac17d87c31c8
		function convert_a(::Type{Symbol}, vec::V, m::SimpleGridWorld) where {V<:AbstractArray}
    actions(m)[convert(Int, first(vec))]
		end


# ╔═╡ e4391014-5e0f-4a4c-898f-156a7a626afa
initialstate_distribution(mdp::SimpleGridWorld) = GWUniform(mdp.size)

# ╔═╡ 5119aaf6-b8de-4287-ad64-66ae18217440
md"# World Rendering [!]"

# ╔═╡ 6f0b2c41-3e1c-49ef-a4d5-f4cae9971fb4
function cell_ctx(xy, size)
    nx, ny = size
    x, y = xy
    return compose(context((x-1)/nx, (ny-y)/ny, 1/nx, 1/ny))
end

# ╔═╡ 8ea7f177-420a-42a5-ace2-eced8061bbf6
function tocolor(r::Float64)
    minr = -10.0
    maxr = 10.0
    frac = (r-minr)/(maxr-minr)
    return get(ColorSchemes.redgreensplit, frac)
end

# ╔═╡ 97e9f5cb-4c59-484b-bda7-3913de903699
tocolor(x) = x

# ╔═╡ 2b898d69-6ac7-42d4-8bfb-6cba3319e7ad
tofunc(m::SimpleGridWorld, f) = f
# tofunc(m::SimpleGridWorld, f) = f+

# ╔═╡ 650a44d7-e929-4720-ae70-4185a681ec45
tofunc(m::SimpleGridWorld, mat::AbstractMatrix) = s->mat[s...]

# ╔═╡ 660ecd7d-40b1-47f4-b18e-171d1b5536d8
tofunc(m::SimpleGridWorld, v::AbstractVector) = s->v[stateindex(m, s)]

# ╔═╡ 56bdce80-ebf7-4e97-8f96-e9125d33696a
const aarrow = Dict(:up=>'⇑', :left=>'⇐', :down=>'⇓', :right=>'⇒')

# ╔═╡ da78ba96-b3b9-4210-8f68-14e9d5e501b1
function POMDPModelTools.render(mdp::SimpleGridWorld, step::Union{NamedTuple,Dict}=(;);
                color = s->reward(mdp, s),
                policy::Union{Policy,Nothing} = nothing
               )

    color = tofunc(mdp, color)

    nx, ny = mdp.size
    cells = []
    for x in 1:nx, y in 1:ny
        cell = cell_ctx((x,y), mdp.size)
        if policy !== nothing
            a = action(policy, GWPos(x,y))
            txt = compose(context(), text(0.5, 0.5, aarrow[a], hcenter, vcenter), stroke("black"))
            compose!(cell, txt)
        end
        clr = tocolor(color(GWPos(x,y)))
        compose!(cell, rectangle(), fill(clr), Compose.stroke("gray"))
        push!(cells, cell)
    end
    grid = compose(context(), linewidth(0.5mm), cells...)
    outline = compose(context(), linewidth(1mm), rectangle(), Compose.stroke("gray"))

    if haskey(step, :s)
        agent_ctx = cell_ctx(step[:s], mdp.size)
        agent = compose(agent_ctx, circle(0.5, 0.5, 0.4), fill("orange"))
    else
        agent = nothing
    end

    sz = min(w,h)
    return compose(context((w-sz)/2, (h-sz)/2, sz, sz), agent, grid, outline)
end


# ╔═╡ f6a0ab11-3222-4937-bb61-b3f3b495ed9f
# begin 
# tocolor(x) = x
# tofunc(m::SimpleGridWorld, f) = f+
# # tofunc(m::SimpleGridWorld, mat::AbstractMatrix) = s->mat[s...]
# # tofunc(m::SimpleGridWorld, v::AbstractVector) = s->v[stateindex(m, s)]
#  aarrow = Dict(:up=>'↑', :left=>'←', :down=>'↓', :right=>'→')
	
# end


# ╔═╡ 37314571-1953-4481-9adf-55c3c3104194


# ╔═╡ fad17323-c7ef-4c75-95c4-8aad523b2068


# ╔═╡ 6d35cafc-8fd5-431b-b9f9-534f2e42ea21
# begin
# 	gw = SimpleGridWorld()
# render(gw, (s=[7,6],))
# 	p = FunctionPolicy(s->:up)
# steps = collect(stepthrough(gw, p, max_steps=100, rng=MersenneTwister(3)));
# render(gw, first(steps))
	
# end
	

# ╔═╡ 2cf74d58-815f-4251-8dbf-98b351649934
md"## Rendering & Policy Generation Testing"

# ╔═╡ c260a691-e550-40fa-b150-476d951d9e0e
testWorld = SimpleGridWorld()

# ╔═╡ 3ddb470b-7674-4083-a6b5-cbafc7a57179
p = FunctionPolicy(s->:up)

# ╔═╡ 5e8ad769-8e94-46af-9e56-464d5f9a4426
is = GWPos(1,1)

# ╔═╡ 7084c7ad-4826-490b-9165-50eebee348c2
	stage = first(stepthrough(testWorld, p, is, max_steps=1))

# ╔═╡ 2d406e66-ccb7-4658-992d-4e1a63333a58
md"## Our Grid World"

# ╔═╡ a9ceb869-bbb5-49d2-b7c4-a024fd6c17c2
b =  render(testWorld, stage, color=s->reward(testWorld,s))

# ╔═╡ 60d28790-dd06-40dd-ac8b-25f85b34334e
md"### How a Random Policiy Looks Like when Declared"

# ╔═╡ dc0d1c8a-0c83-4972-8b99-482a2ebd3faa
g =  render(testWorld, stage,  color=s->reward(testWorld,s), policy=RandomPolicy(SimpleGridWorld()))

# ╔═╡ 9ac7507f-7b0f-4d2b-af53-69bca95e43c9
md"# Final Simulation"

# ╔═╡ 04a73897-1c33-4de0-8cee-f48383cf9ca1
md"## Declar World"

# ╔═╡ d4c1b252-b7eb-467d-bd6a-fe13738510c0
    problem = SimpleGridWorld()

# ╔═╡ dbbad1bf-c158-425f-b1c6-293d23510ecf
md"## Generate Random Policy"

# ╔═╡ 6c060677-fa80-4af2-8bc7-b4f39c9d6ec9
policy = RandomPolicy(problem)

# ╔═╡ c2a27aea-e036-4fa2-9444-0169cf2c6969
md"## Create Simulation History Recorder"

# ╔═╡ a05020d8-b642-48e9-ab60-459cf3b86c13
    sim = HistoryRecorder(rng=MersenneTwister(1), max_steps=1000)

# ╔═╡ d306f696-3eee-4aec-8ce6-8fb000a3bb85
md"## Start Policy Analysis"

# ╔═╡ 4e0d4847-8b28-4f25-ba04-806cf2c7229d
begin 
	
	   hist = simulate(sim, problem, policy, GWPos(1,1))
	
	    for (s, a) in zip(state_hist(hist), action_hist(hist))
		
		@show "State => $s Action $a" #displaying The Transition model in Terminal
		
        td = transition(problem, s, a)
        if td isa SparseCat
            @test sum(td.probs) ≈ 1.0 atol=0.01
            for p in td.probs
                @test p >= 0.0
            end
        end
    end
	
	sv = convert_s(Array{Float64}, GWPos(1,1), problem)
    @test sv == [1.0, 1.0]
	
    sv = convert_s(Array{Float64}, GWPos(5,3), problem)
    @test sv == [5.0, 3.0]
	
    s = convert_s(GWPos, sv, problem)
    @test s == GWPos(5, 3)

    av = convert_a(Array{Float64}, :up, problem)
    @test av == [1.0]
	
    a = convert_a(Symbol, av, problem)
    @test a == :up

    @test has_consistent_transition_distributions(problem)
	
	@show "All Tests Successfully Passed"
	
    pol = FunctionPolicy(x->:up)
    stp = first(stepthrough(problem, pol, "s,a", max_steps=1))
    POMDPModelTools.render(problem, stp)
    POMDPModelTools.render(problem, NamedTuple())
    POMDPModelTools.render(problem, stp, color=s->reward(problem,s))
    POMDPModelTools.render(problem, stp, color=s->rand())
    POMDPModelTools.render(problem, stp, color=s->"yellow")
	
	ss = collect(states(problem)) #collection of all available states
	isd = initialstate(problem)
    # @show	 isd
	for s in ss
		# @show s
        if !isterminal(problem, s)
            @test s in support(isd)
            @test pdf(isd, s) > 0.0
        end
    end
end


# ╔═╡ a98b28e8-724a-4028-9a27-c5fce17298b8


# ╔═╡ a7fb2068-9411-4a75-bcd9-898bea03bfd0
md"# Grid world Banch marks"

# ╔═╡ d9e3e684-9a59-4201-9fbf-4a08d8b1a1ef
begin
	mdp = SimpleGridWorld()
    policy2 = RandomPolicy(mdp, rng=MersenneTwister(7))
    simUlator = RolloutSimulator(max_steps=10_000, rng=MersenneTwister(2))
    @btime simulate($simUlator, $mdp, $policy2)
	
end	

# ╔═╡ Cell order:
# ╠═974e1edc-f837-41d8-9657-65ad57d85419
# ╠═9bab7974-38f5-4b5c-b5b1-4c312dda8303
# ╟─c7d55112-baf5-4a20-96b8-7e38efde74d1
# ╠═9406ecf6-32d1-437a-a702-5043343c9db1
# ╠═80578f8e-f6ab-40f1-8c53-927f8374e7dc
# ╠═d339e7d5-51a5-4d79-8465-88d209947bc4
# ╠═03791fd9-6de2-4330-a3b9-ad916fb90c71
# ╠═a23bd61c-d379-4bad-9f03-4085fe5e7c94
# ╠═99d00db2-1fcf-409e-9555-9fd87c8421e2
# ╠═1c243a50-0002-4239-bbc8-3f5e9f4fd861
# ╠═16afac91-c2b6-43a3-9a5e-3bed5b583c4e
# ╟─c082a2a5-0e12-4e3a-9cbc-3e154e574fa2
# ╠═4f88c935-6f4d-42fd-9433-44fbb07dcdea
# ╠═cf48e641-5d77-4ec7-b231-4e2fadb1c369
# ╠═6ec4e27a-8abc-47e6-ab9c-3bf2bd9a21ac
# ╠═855e26cc-19c0-4e17-962a-e63d2e20d000
# ╠═3e38ec49-785f-4b1b-8660-75c5fa08106b
# ╠═71f09d11-7769-4557-aa09-c36dda1ecb44
# ╠═75ef83ac-6c7a-4618-9f58-574ac8ecaf5c
# ╠═aa0e760c-87e0-4184-97ed-dc53976f3151
# ╠═71e92ceb-b8f3-4383-b55b-986564ae0d99
# ╠═e0659180-b273-4682-8ad1-9a3148ecc229
# ╠═e3cbe85a-8956-4e54-9f8f-541ceebfa263
# ╠═d6523f00-7e4d-4d19-9196-4a3b12c7bbce
# ╠═dae85980-d665-4a84-a1e8-ca17f6e89b5b
# ╠═a7bca195-850a-4cb7-8de5-578c716216cf
# ╠═a57571ec-e1e0-4ac3-ada0-ed932def0822
# ╠═b68377f6-a930-4c79-b2d3-f67d1950dd69
# ╠═37f0a8d7-7395-4db5-8698-79abfbb19ccf
# ╠═c6b13a22-1370-4e96-baaa-8ec3212a1332
# ╠═bacf774f-97c8-4916-8b4b-46072fcef9b9
# ╠═7b235da8-b805-40de-ad2d-b6b5fc4dd36a
# ╠═f5138e9b-e1be-4b6a-8acf-562cb8c2f8fe
# ╠═105c60ea-3137-479e-a1ca-ea1f50e169b7
# ╠═837b9a0b-f2b0-4485-b03e-ac17d87c31c8
# ╠═e4391014-5e0f-4a4c-898f-156a7a626afa
# ╠═5119aaf6-b8de-4287-ad64-66ae18217440
# ╠═da78ba96-b3b9-4210-8f68-14e9d5e501b1
# ╠═6f0b2c41-3e1c-49ef-a4d5-f4cae9971fb4
# ╠═8ea7f177-420a-42a5-ace2-eced8061bbf6
# ╠═97e9f5cb-4c59-484b-bda7-3913de903699
# ╠═2b898d69-6ac7-42d4-8bfb-6cba3319e7ad
# ╠═650a44d7-e929-4720-ae70-4185a681ec45
# ╠═660ecd7d-40b1-47f4-b18e-171d1b5536d8
# ╠═56bdce80-ebf7-4e97-8f96-e9125d33696a
# ╠═f6a0ab11-3222-4937-bb61-b3f3b495ed9f
# ╠═37314571-1953-4481-9adf-55c3c3104194
# ╠═fad17323-c7ef-4c75-95c4-8aad523b2068
# ╟─6d35cafc-8fd5-431b-b9f9-534f2e42ea21
# ╠═2cf74d58-815f-4251-8dbf-98b351649934
# ╠═c260a691-e550-40fa-b150-476d951d9e0e
# ╠═3ddb470b-7674-4083-a6b5-cbafc7a57179
# ╠═5e8ad769-8e94-46af-9e56-464d5f9a4426
# ╠═7084c7ad-4826-490b-9165-50eebee348c2
# ╟─2d406e66-ccb7-4658-992d-4e1a63333a58
# ╠═a9ceb869-bbb5-49d2-b7c4-a024fd6c17c2
# ╠═60d28790-dd06-40dd-ac8b-25f85b34334e
# ╠═dc0d1c8a-0c83-4972-8b99-482a2ebd3faa
# ╟─9ac7507f-7b0f-4d2b-af53-69bca95e43c9
# ╟─04a73897-1c33-4de0-8cee-f48383cf9ca1
# ╠═d4c1b252-b7eb-467d-bd6a-fe13738510c0
# ╟─dbbad1bf-c158-425f-b1c6-293d23510ecf
# ╠═6c060677-fa80-4af2-8bc7-b4f39c9d6ec9
# ╟─c2a27aea-e036-4fa2-9444-0169cf2c6969
# ╠═a05020d8-b642-48e9-ab60-459cf3b86c13
# ╟─d306f696-3eee-4aec-8ce6-8fb000a3bb85
# ╠═4e0d4847-8b28-4f25-ba04-806cf2c7229d
# ╠═a98b28e8-724a-4028-9a27-c5fce17298b8
# ╟─a7fb2068-9411-4a75-bcd9-898bea03bfd0
# ╠═d9e3e684-9a59-4201-9fbf-4a08d8b1a1ef
