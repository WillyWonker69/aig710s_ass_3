### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 1ead6170-dd0b-11eb-3c53-b7a8ad140554
begin
	using Pkg
	# Pkg.add(url="https://github.com/QuantEcon/Games")
	Pkg.activate("Project.toml")
	using PlutoUI

end


# ╔═╡ 91784b70-849c-4b6f-85c4-58f4dc22ec69
using Games

# ╔═╡ f5456402-641e-4a9e-9355-eb7da9631d95
md"# Nash's Theorem (1950)"

# ╔═╡ 6004de96-d9cb-4acc-85e6-c2e7658c00bb
md"### Every finite game has at least one Nash equilibrium  
#### in (pure or) mixed strategies."

# ╔═╡ f1ce42da-3304-4b70-9e8d-99efd008ad99
md"# Normal Form Game"

# ╔═╡ 7aff0335-f82a-4665-b22c-bcec4f35e26b
md"## N-Player Game 
## A triplet g=(I,(Aᵢ)i∈I,(uᵢ)i∈I) where:

### I={1,…,N} is the set of players,
### Aᵢ={1,…,ni} is the set of actions of player i∈I,
### uᵢ:Aᵢ×Aᵢ+1×⋯×Aᵢ₊ₙ−₁→R Player payoff function,i∈I,"

# ╔═╡ 358e1c47-062c-4706-93b3-0c6818e4186c
md"# Games Defininations"

# ╔═╡ 2f4f9265-dd69-4244-bce2-154692eccffe
md"## Coing Toss || Matching Pennies
### 2x2 normal Form Game"

# ╔═╡ ae1dc4c5-ea68-4f05-8083-cb2bcdada837
md"### Payoff profiles for each Action."

# ╔═╡ 841600ff-ef82-45d8-9ca3-fa85d0809a92
begin
	MP_Game_PayOffMatrix = Array{Float64}(undef, 2, 2, 2)
	MP_Game_PayOffMatrix[1, 1, :] = [1, -1] 
	MP_Game_PayOffMatrix[1, 2, :] = [-1, 1]
	MP_Game_PayOffMatrix[2, 1, :] = [-1, 1]
	MP_Game_PayOffMatrix[2, 2, :] = [1, -1]
	MatchingPannies_Game = NormalFormGame(MP_Game_PayOffMatrix)
	
end

# ╔═╡ 90a4de88-bd30-4d43-aa09-35673fee456a
md"###  Player One"

# ╔═╡ 67e84ad9-578a-4e65-9c70-16effb6e4085
MatchingPannies_Game.players[1]

# ╔═╡ c2199309-3633-4cfc-8550-5f095f352e85
md"###  Player Two"

# ╔═╡ 526e2216-9e54-4d9b-93b0-26fa7d3b7637
MatchingPannies_Game.players[2]

# ╔═╡ 04db5f48-f450-4602-8dcc-d75d69b5c711
md"## Coordination Game
### 2x2 normal Form Game"

# ╔═╡ c5c447a8-dd1b-42bc-8dd7-90dedf5d8948
coordination_game_matrix = [4 0;3 2]  # square matrix

# ╔═╡ d8d1a74a-890e-4caa-bde4-9c490c47290f
coordinationGame = NormalFormGame(coordination_game_matrix)

# ╔═╡ f0e9b8a7-01c8-469f-bb04-0f9004c7730c
md"### Player Pay Offs"

# ╔═╡ d29ad8b2-2f17-4a20-be91-a723a103912b
coordinationGame.players[1].payoff_array

# ╔═╡ bb1ae709-462c-4496-915f-0327bb34b931
coordinationGame.players[2].payoff_array

# ╔═╡ 65b8a181-51ee-426a-9d92-d40f12c4681f
md"## Rock-Paper-Scissors
### 3x3 normal Form Game"

# ╔═╡ 63b725e9-f82d-46ed-9575-bdda1b6482c9
begin
RPS_matrix = [0 -1 1;  1 0 -1; -1 1 0]
g_RPS = NormalFormGame(RPS_matrix)
end

# ╔═╡ b0a0ac76-a9db-42ab-8ed9-62255c39aaa4
md"# N-Player Game"

# ╔═╡ e76627c0-b7a8-4bc4-9e95-23d6025d28e0
md"## Cournot Game"

# ╔═╡ 4949c196-bcba-4663-99e6-f48a28c3d1fd
# a inverse demand function
# c marginal cost
# N => Number of players
# q_grid possible quantity values

function cournotGameGeneration(a::Real, c::Real, ::Val{N}, q_grid::AbstractVector{T}) where {N,T<:Real}
    nums_actions = ntuple(x->length(q_grid), Val(N))
    S = promote_type(typeof(a), typeof(c), T)
    payoff_array= Array{S}(undef, nums_actions)
    for I in CartesianIndices(nums_actions)
        Q = zero(S)
        for i in 1:N
            Q += q_grid[I[i]]
        end
        payoff_array[I] = (a - c - Q) * q_grid[I[1]]
    end
    players = ntuple(x->Player(payoff_array), Val(N))
    return NormalFormGame(players)
end

# ╔═╡ 630bd9bf-4ddf-471d-8743-0f3368a3205d
begin
a, c = 80, 20
N = 3
q_grid = [10, 15]  # [1/3 of Monopoly quantity, Nash equilibrium quantity]

cournotGame = cournotGameGeneration(a, c, Val(N), q_grid)
end

# ╔═╡ 1abf56c2-3eb5-42c3-b281-6439a5b609af
md"# Player Creation"

# ╔═╡ bb0e279a-9321-48e0-9ac0-bd25f4d9ec0f
begin
	player1 = Player([3 1; 0 2])
	player2 = Player([2 0; 1 3])
end

# ╔═╡ fa5c954a-928f-4baa-9fdd-34d1d1a8009a
player1.payoff_array

# ╔═╡ ff4432d4-a246-4383-9843-cb4e64b3cf68
player2.payoff_array

# ╔═╡ b4fe4786-50f4-4ac3-a18a-f64fe8afc0e9
md"# Finding Best Responses"

# ╔═╡ 1cf4d3f5-3b35-4ae3-b13e-7f2a9085b7cc
best_response(MatchingPannies_Game.players[1], 2) 
# This function responds with the Best Responst to Player 2

# ╔═╡ c98c7d27-1381-4618-8c0b-559ce99fc47f
function sequential_best_response(g::NormalFormGame{N};
                                  init_actions::Vector{Int}=ones(Int, N),
                                  tie_breaking=:smallest,
                                  verbose=true) where N
    a = copy(init_actions)
    if verbose
        println("init_actions: $a")
    end
    
    new_a = Array{Int}(undef, N)
    max_iter = prod(g.nums_actions)
    
    for t in 1:max_iter
        copyto!(new_a, a)
		
        for (i, player) in enumerate(g.players)
            if N == 2
                a_except_i = new_a[3-i]
            else
                a_except_i = (new_a[i+1:N]..., new_a[1:i-1]...)
            end
            new_a[i] = best_response(player, a_except_i,
                                     tie_breaking=tie_breaking)
            if verbose
				# payOff = g.players[i]
				# println("Player Payoff :> $payOff")
                println("player $i: $new_a")
            end
        end
        if new_a == a
            return a
        else
            copyto!(a, new_a)
        end
    end
    
    println("No pure Nash equilibrium found")
    return a
end

# ╔═╡ 125105cd-26d5-4d5b-b0fd-75e5d23ea0b4
begin
a_star = sequential_best_response(MatchingPannies_Game)  
end

# ╔═╡ 04b08070-2238-4986-ab75-19fcdf6bdae9


# ╔═╡ Cell order:
# ╠═1ead6170-dd0b-11eb-3c53-b7a8ad140554
# ╠═91784b70-849c-4b6f-85c4-58f4dc22ec69
# ╟─f5456402-641e-4a9e-9355-eb7da9631d95
# ╟─6004de96-d9cb-4acc-85e6-c2e7658c00bb
# ╟─f1ce42da-3304-4b70-9e8d-99efd008ad99
# ╟─7aff0335-f82a-4665-b22c-bcec4f35e26b
# ╟─358e1c47-062c-4706-93b3-0c6818e4186c
# ╟─2f4f9265-dd69-4244-bce2-154692eccffe
# ╠═ae1dc4c5-ea68-4f05-8083-cb2bcdada837
# ╠═841600ff-ef82-45d8-9ca3-fa85d0809a92
# ╟─90a4de88-bd30-4d43-aa09-35673fee456a
# ╠═67e84ad9-578a-4e65-9c70-16effb6e4085
# ╟─c2199309-3633-4cfc-8550-5f095f352e85
# ╠═526e2216-9e54-4d9b-93b0-26fa7d3b7637
# ╟─04db5f48-f450-4602-8dcc-d75d69b5c711
# ╠═c5c447a8-dd1b-42bc-8dd7-90dedf5d8948
# ╠═d8d1a74a-890e-4caa-bde4-9c490c47290f
# ╟─f0e9b8a7-01c8-469f-bb04-0f9004c7730c
# ╠═d29ad8b2-2f17-4a20-be91-a723a103912b
# ╠═bb1ae709-462c-4496-915f-0327bb34b931
# ╟─65b8a181-51ee-426a-9d92-d40f12c4681f
# ╠═63b725e9-f82d-46ed-9575-bdda1b6482c9
# ╟─b0a0ac76-a9db-42ab-8ed9-62255c39aaa4
# ╟─e76627c0-b7a8-4bc4-9e95-23d6025d28e0
# ╠═4949c196-bcba-4663-99e6-f48a28c3d1fd
# ╠═630bd9bf-4ddf-471d-8743-0f3368a3205d
# ╟─1abf56c2-3eb5-42c3-b281-6439a5b609af
# ╠═bb0e279a-9321-48e0-9ac0-bd25f4d9ec0f
# ╠═fa5c954a-928f-4baa-9fdd-34d1d1a8009a
# ╠═ff4432d4-a246-4383-9843-cb4e64b3cf68
# ╠═b4fe4786-50f4-4ac3-a18a-f64fe8afc0e9
# ╠═1cf4d3f5-3b35-4ae3-b13e-7f2a9085b7cc
# ╠═c98c7d27-1381-4618-8c0b-559ce99fc47f
# ╠═125105cd-26d5-4d5b-b0fd-75e5d23ea0b4
# ╠═04b08070-2238-4986-ab75-19fcdf6bdae9
